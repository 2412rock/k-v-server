===============================
Key-value store Server-Client
===============================

This is the implementation of a multi-threaded server application, that supports multiple clients. 
The objective is to support multiple concurrent clients modifying data, in a key-value data structure. Similar to Redus and
Memcached, this application can be used for caching.

A key-value store allows users to store and retrieve data from a server over the network. The data (value)
is addressed by a string (the key). Conceptually they are very simple, and only have 3 operations: SET,
GET, and DEL. A SET inserts or modifies data into the store, whereas GET retrieves previously stored
data. Finally, DEL simply removes a stored entry. 

Internally the key-value store uses a hashtable, where they key is hashed to decide the index/bucket in
the hashtable. Each bucket of the hashtable consists of a linked list of all items that map to that bucket.

The key-value store uses a simple text-based protocol. After opening a connection, a user can issue any of the following commands. Once a command is completed, another command can be issued over the same connection. 
All commands and responses are terminated by a newline (``\n``).

Keys can only consist of printable ascii characters except for whitespace. Values can contain any character (including ``\n`` and ``\0``), and must therefore always be accompanied by a size field.

The general format of every command is as follows:
::

	<command> [<key>] [<payload_len>]\n
	[<payload>\n]

Where <command> must be one of SET|GET|DEL|RESET|PING|DUMP|EXIT|SETOPT. 
Values in brackets are optional depending on the command. When <payload_len> is omitted or 0, <payload> *and* its accompanying ``\n`` are not sent.

For every command the server will respond with a response in the following format:
::

	<status> <code> <payload_len>\n
	[<payload>\n]

If <payload_len> is 0, payload (and its accompanying ``\n``) is omitted.
Possible responses are:

+--------+--------------+
| status | code         |
+========+==============+
| 0 	 | OK           |
+--------+--------------+
| 1      | KEY_ERROR    |
+--------+--------------+
| 2      | PARSING_ERROR|
+--------+--------------+
| 3      | STORE_ERROR  |
+--------+--------------+
| 4      | SETOPT_ERROR |
+--------+--------------+
| 5      | UNK_ERROR    |
+--------+--------------+


