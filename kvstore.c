#include <semaphore.h>
#include <assert.h>
#include "server_utils.h"
#include "common.h"
#include "request_dispatcher.h"
#include "hash.h"
#include "kvstore.h"
#include <pthread.h>
// DO NOT MODIFY THIS.
// ./check.py assumes the hashtable has 256 buckets.
// You should initialize your hashtable with this capacity.
#define HT_CAPACITY 256

pthread_rwlock_t ht_entry_lock[256];

int element_exists_in_list(hash_item_t **head, struct request *request)
{
    hash_item_t *current = *head;
    while (current != NULL)
    {
        if (strncmp(current->key, request->key, request->key_len) == 0)
        {
            return 1;
        }
        else
        {
            current = current->next;
        }
    }
    return 0;
}

int write_entry(hash_item_t **head, struct request *request, char *buf)
{
    hash_item_t *current = *head;
    while (current != NULL)
    {
        if (strncmp(current->key, request->key, request->key_len) == 0)
        {
            current->value = malloc((request->msg_len));
            for (unsigned i = 0; i < request->msg_len; i++)
            {
                current->value[i] = buf[i];
            }
            return 1;
        }
        else
        {
            current = current->next;
        }
    }
    return 0;
}

int overwrite_entry(hash_item_t **head, struct request *request, char *buf)
{
    hash_item_t *current = *head;
    while (current != NULL)
    {
        if (strncmp(current->key, request->key, request->key_len) == 0)
        {
            size_t len = request->msg_len;
            current->value = realloc(current->value,len);
            for (unsigned i = 0; i < request->msg_len; i++)
            {
                current->value[i] = buf[i];
            }
            current->value_size = len;
            return 1;
        }
        else
        {
            current = current->next;
        }
    }
    return 0;
}

int set_request(int socket, struct request *request)
{
    size_t len = 0;
    size_t expected_len = request->msg_len;
    unsigned bucket = hash(request->key) % 256;
    
    
    // 1. Lock the hashtable entry. Create it if the key is not in the store.
    
    char *buf = malloc(expected_len);
    
   
    int exists = element_exists_in_list(&ht->items[hash(request->key) % 256], request);
    if(exists==1)
    {
        pthread_rwlock_wrlock(&ht_entry_lock[bucket]);
    }
   
    while (len < expected_len)
    {
        
        int read_status = read_payload(socket, request, expected_len, buf); //blocks
        
        if (read_status == -1)
        {
            send_response(socket, KEY_ERROR, 0, NULL);
            if(exists == 1)
            {
                pthread_rwlock_unlock(&ht_entry_lock[bucket]);
            }
            // pthread_rwlock_unlock(&ht_entry_lock[bucket]);
            return 0;
        }
        if (exists == 0)
        {
            pthread_rwlock_wrlock(&ht_entry_lock[bucket]);
            hash_item_t **item_head = &ht->items[hash(request->key) % 256];
            hash_item_t *new_item = malloc(sizeof(hash_item_t));
            new_item->key = malloc((request->key_len));
           // new_item->user = malloc(sizeof(struct user_item));
          //  new_item->user->entry_mutex = malloc(sizeof(pthread_mutex_t));
          //  pthread_mutex_init(new_item->user->entry_mutex,NULL);
            strncpy(new_item->key, request->key, request->key_len);
            new_item->value_size = request->msg_len;
            new_item->next = *item_head;
            *item_head = new_item;
            //exists = 1;
        }
        len += read_status;
        if (exists == 0)
        {
            write_entry(&ht->items[hash(request->key) % 256], request, buf);
        }
        else
        {
            overwrite_entry(&ht->items[hash(request->key) % 256], request, buf);
        }
        buf = realloc(buf, expected_len);
        if(exists == 0)
        {
            exists = 1;
        }
    }
    check_payload(socket, request, expected_len);
    send_response(socket, OK, 0, NULL);
    pthread_rwlock_unlock(&ht_entry_lock[bucket]);
    return len;
}

char *get_element_value(hash_item_t **head, struct request *request, size_t *size)
{
    hash_item_t *current = *head;
    while (current != NULL)
    {
        if (strncmp(current->key, request->key, request->key_len) == 0)
        {
            char *buf = malloc(current->value_size);
            *size = current->value_size;
            for (unsigned i = 0; i < current->value_size; i++)
            {
                buf[i] = current->value[i];
            }
            return buf;
        }
        else
        {
            current = current->next;
        }
    }
    return NULL;
}

int get_request(int socket, struct request *request)
{
    unsigned bucket = hash(request->key) % 256;
    if(pthread_rwlock_tryrdlock(&ht_entry_lock[bucket]) != 0)
    {
        send_response(socket, KEY_ERROR, 0, NULL);
        return 1;
    }

    size_t size = 0;
    char *buf = get_element_value(&ht->items[hash(request->key) % 256], request, &size);
    if (buf != NULL)
    {
        pthread_rwlock_unlock(&ht_entry_lock[bucket]) ;
        send_response(socket, OK, size, buf);
    }

    else
    {
        pthread_rwlock_unlock(&ht_entry_lock[bucket]) ;
        send_response(socket, KEY_ERROR, 0, NULL);
    }
    
    return 1;
}

int delete (hash_item_t **head, struct request *request)
{
    hash_item_t *current = *head;
    while (current != NULL)
    {
        if (strncmp(current->key, request->key, request->key_len) == 0)
        {
            hash_item_t *next_node = (*head)->next;
            free(*head);
            *head = next_node;
            return 0;
        }
        else
        {
            current = current->next;
        }
    }
    return -1;
}

int del_request(int socket, struct request *request)
{
    unsigned bucket = hash(request->key) % 256;
    pthread_rwlock_wrlock(&ht_entry_lock[bucket]);
    
    int r = delete (&ht->items[hash(request->key) % 256], request);
    if (r == 0)
    {
        send_response(socket, OK, 0, NULL);
    }
    else
    {
        send_response(socket, KEY_ERROR, 0, NULL);
    }
    pthread_rwlock_unlock(&ht_entry_lock[bucket]);
    return 1;
}

void *main_job(void *arg)
{
    int method;
    struct conn_info *conn_info = arg;
    struct request *request = allocate_request();
    request->connection_close = 0;
    pr_info("Starting new session from %s:%d\n",
            inet_ntoa(conn_info->addr.sin_addr),
            ntohs(conn_info->addr.sin_port));
    do
    {
        method = recv_request(conn_info->socket_fd, request);
        switch (method)
        {
        case SET:
            set_request(conn_info->socket_fd, request);
            break;
        case GET:
            get_request(conn_info->socket_fd, request);
            break;
        case DEL:
            del_request(conn_info->socket_fd, request);
            break;
        case RST:
            send_response(conn_info->socket_fd, OK, 0, NULL);
            break;
        }
        if (request->key)
        {
            free(request->key);
        }
    } while (!request->connection_close);
    close_connection(conn_info->socket_fd);
    free(request);
    free(conn_info);
    pthread_exit(NULL);
    return (void *)NULL;
}

int main(int argc, char *argv[])
{
    for(unsigned i=0; i<256;i++)
    {
        pthread_rwlock_init(&ht_entry_lock[i],NULL);
    }
    int listen_sock;
    listen_sock = server_init(argc, argv);
    ht = malloc(sizeof(hash_item_t));
    ht->capacity = HT_CAPACITY;
    hash_item_t *link[256] = {0};
    ht->items = link;
    for (;;)
    {
        struct conn_info *conn_info =
            calloc(1, sizeof(struct conn_info));
        if (accept_new_connection(listen_sock, conn_info) < 0)
        {
            error("Cannot accept new connection");
            free(conn_info);
            continue;
        }
        pthread_t tid;
        pthread_create(&tid, NULL, main_job, conn_info);
    }
    return 0;
}